require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :domain, '54.194.107.3'
set :user, 'ms'
set :deploy_to, '/mnt/eb_store/admin'
set :repository, 'https://tosyn@bitbucket.org/tosyn/admin.git'
set :branch, 'master'
set :term_mode, :system 	#This line is required for deploy to prompt for
							#git username and password when cloning git repo
							# by default this is set to 'set :term_mode, :pretty'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml']

# Optional settings:
#   set :user, 'traffic'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use[ruby-1.9.3-p125@default]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]
  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]
  queue! %[touch "#{deploy_to}/shared/config/.env"]
  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]
  queue! %[mkdir -p "#{deploy_to}/scripts"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/scripts"]
  queue! %[mkdir -p "#{deploy_to}/shared/vendor"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/storage"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/app"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/framework"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/framework/cache"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/framework/sessions"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/framework/views"]
  queue! %[mkdir -p "#{deploy_to}/shared/storage/logs"]
  queue! %[mkdir -p "#{deploy_to}/shared/cache"]

end

desc "Deploy application"
task :deploy => :environment do
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'

    to :launch do
        invoke :'symlink_storage'
        invoke :'symlink_env'
        invoke :'reload_apache'
    end
  end
end

desc "Reload Apache"
task :reload_apache => :environment do
  queue %[echo "-----> Reloading Apache"]
  queue "sudo service apache2 reload"
end

desc "Symlink Strorage dir"
task :symlink_storage => :environment do
  queue %[echo "-----> Creating storage symlink"]
  queue "ln -s #{deploy_to}/shared/storage #{deploy_to}/current/storage"
end

desc "Symlink Vendor dir"
task :symlink_vendor => :environment do
  queue %[echo "-----> Creating vendor symlink"]
  queue "ln -s #{deploy_to}/shared/vendor #{deploy_to}/current/vendor"
end

desc "symlink bootstrap cache dir"
task :symlink_bootstrap_cache => :environment do
  queue %[echo "-----> Creating bootstrap cache dir symlink"]
  queue "ln -s #{deploy_to}/shared/cache #{deploy_to}/current/bootstrap/cache"
end

desc "Symlink env"
task :symlink_env => :environment do
  queue %[echo "-----> Creating env symlink"]
  queue "ln -s #{deploy_to}/shared/config/.env #{deploy_to}/current/.env"
end

desc "Install 3rd party libraries"
task :composer_update => :environment do
  queue %[echo "-----> Running composer update"]
  queue "cd #{deploy_to}/current && composer update --no-dev"
end

desc "Rollback release."
task :rollback => :environment do
  queue %[echo "-----> Start release rollback"]
  queue %[echo "-----> Check if we have old release to rollback to"]
  queue %[if [ $(ls #{deploy_to}/releases | wc -l) -gt 1 ]; then echo "-----> Relink to previous release" && unlink #{deploy_to}/current && ln -s #{deploy_to}/releases/"$(ls -tr #{deploy_to}/releases | tail -2 | head -1)" #{deploy_to}/current && echo "-----> Remove old release" && rm -rf #{deploy_to}/releases/"$(ls -tr #{deploy_to}/releases | tail -1)" && echo "$(ls -tr #{deploy_to}/releases | tail -1)" > #{deploy_to}/last_version && echo "-----> Done. Rollback to release$(cat #{deploy_to}/last_version)" ; else echo "-----> No old release to rollback to" ; fi]
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers
