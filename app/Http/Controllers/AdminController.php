<?php

namespace App\Http\Controllers;

use Log;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin as Admin;
use App\Models\Enquiry as Enquiry;
use App\Models\Page as Page;
use App\Models\Article as Article;
use App\Models\AdminRole as AdminRole;
use App\Models\Role as Role;

/**
 *
 */
class AdminController extends Controller
{
    /**
     * index.
     */
    public function index()
    {
        // check if user is authenticated
        $this->isAuthorized();

        return view('index', ['title' => 'Dashboard']);
    }

    /**
     * login description.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function login(Request $request)
    {
        // check if user have been authenticated
        if ($this->isAuthenticated()) {
            return redirect('/')->with('notice', 'Welcome '.Session::get('username'));
        }

        // Check if form is submitted
        if ($request->isMethod('post')) {
            // Validate input
            $this->validate($request, array(
                'username' => 'required|alpha_num',
                'password' => 'required',
            ));

            // login details
            $username = $request->input('username');
            $password = $request->input('password');

            // validate username and password
            $admin = Admin::where('username', $username)->first();

            if ($admin && password_verify($password, $admin->hash)) {
                if (!$admin->active)
                  return redirect('/login')->with('notice', "Account has been suspended");
                else {
                  // get user Role
                  $user_role = $admin->adminRole;
                  $role_name = $user_role ? $user_role->name : null;
                  $this->setAuthenticationDetails($username, $role_name);
                }

                return redirect('/')->with('notice', "Welcome $username");
            } else {
                $request->session()->flash('notice', 'invalid username or password');
            }
        }

        return view('login');
    }

    /**
     * Set authentication data to session.
     *
     * @param [string] $username [description]
     */
    private function setAuthenticationDetails($username, $role_name)
    {
        Session::set('username', $username);
        Session::set('role', $role_name);
    }

    /**
     * isAthenticated - check if user have been authenticated.
     *
     * @return bool [description]
     */
    private function isAuthenticated()
    {
        if (Session::has('username') && Session::get('username') !== null) {
            return true;
        } else {
            return false;
        }
    }

    public function logout()
    {
        Session::flush();

        return redirect('/login');
    }

    public function user()
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get admin users
        $users = Admin::all();

        return view('user', ['title' => 'Users', 'users' => $users]);
    }

    public function role()
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get roles
        $roles = Role::all();

        return view('role', ['title' => 'Roles', 'roles' => $roles]);
    }

    public function page()
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get enquires
        $pages = Page::all();

        return view('page', ['title' => 'Pages', 'pages' => $pages]);
    }

    public function enquiry()
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get enquires
        $enquiries = Enquiry::all();

        return view('enquiry', ['title' => 'Enquiries', 'enquiries' => $enquiries]);
    }

    public function article()
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get article
        $articles = Article::all();

        return view('article', ['title' => 'Articles', 'articles' => $articles]);
    }

    /**
     * authCheckPoint - check if user is logged in
     * otherwise redirect to login page.
     */
    private function isAuthorized()
    {
        // check if user is authenticated
        if (!$this->isAuthenticated()) {
            Redirect::to('/login')->send();
        }
    }

    /**
     * suspend user.
     *
     * @param int $id [user id]
     *
     * @return json {success:boolean, message:string}
     */
    public function suspendUser($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // retrieve user by id
        $user = Admin::find($id);

        // set user active state to false
        if ($user) {
            $user->active = false;
            if ($user->save()) {
                $response = ['success' => true, 'message' => 'user suspended'];
            } else {
                $response['message'] = 'there was an error';
            }
        } else {
            $response['message'] = 'user not found';
        }

        return json_encode($response);
    }

    /**
     * activate user.
     *
     * @param int $id [user id]
     *
     * @return json {success:boolean, message:string}
     */
    public function activateUser($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // retrieve user by id
        $user = Admin::find($id);

        // set user active state to true
        if ($user) {
            $user->active = true;
            if ($user->save()) {
                $response = ['success' => true, 'message' => 'user activated'];
            } else {
                $response['message'] = 'there was an error';
            }
        } else {
            $response['message'] = 'user not found';
        }

        return json_encode($response);
    }

    /**
     * updateUser update user.
     *
     * @return Json {success:boolean, message:string}
     */
    public function updateUser()
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // get input
        $id = \Request::input('id');
        $field = \Request::input('columnName');
        $value = \Request::input('value');

        // process request
        switch (strtolower($field)) {
          case 'role':
              // validate input and update role
              if (!$role = Role::find($value)) {
                  $response['message'] = "$value role does not exist";
              } elseif (!AdminRole::where('admin_id', $id)->first()) {
                  try {
                      // assign role to user
                    AdminRole::insert($id, $value);
                      $response = ['success' => true, 'message' => 'user role updated'];
                  } catch (Exception $e) {
                      Log::error(__METHOD__.' - '.$e->getMessage());
                      $response['message'] = 'Exception error';
                  }
              } elseif (AdminRole::where('admin_id', $id)->update(['role_id' => $value])) {
                  $response = ['success' => true, 'message' => 'user role updated'];
              } else {
                  $response['message'] = 'user role could not be updated';
              }
            break;

          default:
              $response['message'] = "$field can not be updated";
            break;
        }

        // send response
        return json_encode($response);
    }

    /**
     * listRoles get list of roles.
     *
     * @return array
     */
    public function listRoles()
    {
        // check if user is authenticated
        $this->isAuthorized();

        $data = [];

        // get roles
        $roles = Role::all();

        // set roles data array
        foreach ($roles as $role) {
            $data[$role->id] = $role->name;
        }

        // send response
        return json_encode($data);
    }

    /**
     * deleteUser delete user.
     *
     * @return Json
     */
    public function deleteUser($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // retrieve user by id
        $user = Admin::find($id);

        // delete user
        if (!$user) {
            $response['message'] = 'user not found';
        } elseif ($user->delete()) {
            $response = ['success' => true, 'message' => 'user deleted'];
        } else {
            $response['message'] = 'error deleting user';
        }

        return json_encode($response);
    }

    /**
     * Add new admin user.
     */
    public function addUser(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // validate input
        $validation = \Validator::make($request->all(), [
            'username' => 'required|alpha_dash|min:3|unique:admins,username',
            'role_id' => 'required|numeric',
            'password' => 'required|confirmed|min:5',
        ]);

        if ($validation->fails()) {
            $errors = [];
            // normalise error messages array
            $messages = $validation->messages()->toArray();
            foreach ($messages as $message) {
                foreach ($message as $value) {
                    $errors[] = $value;
                }
            }

            // set response message
            $response['message'] = implode('<br />', $errors);
        } else {
            try {
                // start transaction
              \DB::beginTransaction();

              // add admin user
              $user = Admin::insert($request->input('username'), $request->input('password'));

              // add admin user role
              $user_role = AdminRole::insert($user->id, $request->input('role_id'));
                $response = ['success' => true, 'message' => 'New user successfully added'];

              // commit transaction
              \DB::commit();
            } catch (\Exception $e) {
                // commit transaction
                \DB::rollback();
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'exception error';
            }
        }

        return json_encode($response);
    }

    /**
     * updatePage - update page note.
     *
     * @return json {success:boolean, message:string}
     */
    public function updatePage(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // get input
        $id = $request->input('id');
        $field = $request->input('columnName');
        $value = $request->input('value');

        // process request
        switch (strtolower($field)) {
          case 'note':
              // TODO: validate input

              // update note
              if (Page::where('id', $id)->update(['note' => $value])) {
                  $response = ['success' => true, 'message' => 'Page note updated'];
              } else {
                  $response['message'] = 'Page note could not be updated';
              }
            break;
          default:
              $response['message'] = "$field can not be updated";
            break;
        }

        // send response
        return json_encode($response);
    }

    /**
     * updateRole - update role.
     *
     * @return json {success:boolean, message:string}
     */
    public function updateRole(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // get input
        $id = $request->input('id');
        $field = $request->input('columnName');
        $value = $request->input('value');

        // process request
        switch (strtolower($field)) {
          case 'note':
              // validate and process input
              $validation = \Validator::make($request->all(), [
                  'value' => 'required|string',
              ]);

              // retrieve and normalise validation error messages
              if ($validation->fails()) {
                  $errors = [];
                  $messages = $validation->messages()->toArray();
                  foreach ($messages as $message) {
                      foreach ($message as $error) {
                          $errors[] = $error;
                      }
                  }
                  $response['message'] = implode('<br />', $errors);
              } elseif (Role::where('id', $id)->update(['note' => $value])) {
                  $response = ['success' => true, 'message' => 'Role note updated'];
              } else {
                  $response['message'] = 'Role note could not be updated';
              }
            break;
          default:
              $response['message'] = $request->input('columnName')."$field can not be updated";
            break;
        }

        // send response
        return json_encode($response);
    }

    /**
     * deleteRole .
     *
     * @return Json {success:boolean, message:string}
     */
    public function deleteRole($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // retrieve role by id
        $role = Role::find($id);

        // delete role
        if (!$role) {
            $response['message'] = 'role does not exist';
        } elseif ($role->delete()) {
            $response = ['success' => true, 'message' => 'role deleted'];
        } else {
            $response['message'] = 'error deleting role';
        }

        return json_encode($response);
    }

    /**
     * Add new role.
     *
     * @return json {success:boolean, message:string}
     */
    public function addRole(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // validate input
        $validation = \Validator::make($request->all(), [
            'name' => 'required|alpha_dash|min:3|unique:roles,name',
            'note' => 'required|string',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }

            $response['message'] = implode('<br />', $errors);
        } else {
            try {
                // add role
                Role::insert($request->input('name'), $request->input('note'));
                $response = ['success' => true, 'message' => 'New role added'];
            } catch (\Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'exception error'.$e->getMessage();
            }
        }

        return json_encode($response);
    }

    /**
     * deleteRoleBatch - delete specified list of roles.
     *
     * @return json {success:boolean, message:string}
     */
    public function deleteRoleBatch(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // validate
        $validation = \Validator::make($request->all(), [
            'ids' => 'required',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }
            $response['message'] = implode(', ', $errors);
        } else {
            try {
                // do batch delete
            Role::destroy($request->input('ids'));
                $response = ['success' => true, 'message' => 'Role(s) deleted'];
            } catch (Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'Exception error';
            }
        }

        return json_encode($response);
    }

    /**
     * deleteUserBatch - delete specified list of users.
     *
     * @return json {success:boolean, message:string}
     */
    public function deleteUserBatch(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // validate
        $validation = \Validator::make($request->all(), [
            'ids' => 'required',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }
            $response['message'] = implode(', ', $errors);
        } else {
            try {
                // do batch delete
            Admin::destroy($request->input('ids'));
                $response = ['success' => true, 'message' => 'User(s) deleted'];
            } catch (Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'Exception error';
            }
        }

        return json_encode($response);
    }

    /**
     * updateArticle - update article.
     *
     * @return json {success:boolean, message:string}
     */
    public function updateArticle(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'update failed'];

        // validate
        $validation = \Validator::make($request->all(), [
            'id' => 'required',
            'columnName' => 'required|in:Author,Title,Image Source,Description',
            'value' => 'required',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }
            $response['message'] = implode(',', $errors);
        } else {
            // get input
          $id = $request->input('id');
            $field = str_replace(' ', '_', strtolower($request->input('columnName')));
            $value = $request->input('value');

            try {
                // update Article
            Article::where('id', $id)->update([$field => $value]);
                $response = ['success' => true, 'message' => 'Article updated'];
            } catch (Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'Exception error';
            }
        }

        // send response
        return json_encode($response);
    }

    /**
     * Story - display article content.
     */
    public function story($id, $file)
    {
        // confirm article exist
        $article = Article::where('id', $id)->where('filename', $file)->first();

        // check if article exist
        $path = base_path().'/resources/views/partials/articles/';
        if ($article && is_readable($path.$file.'.blade.php')) {
            // render view
            return view('story', ['article' => $article]);
        }
        // return 404 if article is not found
        abort(404);
    }

    /**
     * deleteArticlesBatch - delete specified list of articles.
     *
     * @return json {success:boolean, message:string}
     */
    public function deleteArticleBatch(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // validate
        $validation = \Validator::make($request->all(), [
            'ids' => 'required',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }
            $response['message'] = implode(', ', $errors);
        } else {
            try {
                // TODO: Get filenames for articles-directory clean up

                // do batch delete
                Article::destroy($request->input('ids'));
                $response = ['success' => true, 'message' => 'Article(s) deleted'];

                // TODO: Using Queue - schedule deleted article(s) file for cleanup
            } catch (Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'Exception error';
            }
        }

        return json_encode($response);
    }

    /**
     * publish article.
     *
     * @return json
     */
    public function publish(Request $request)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // check if user has permision to publish article
        if (!Session::has('role') || (Session::has('role') && !in_array(Session::get('role'), ['admin', 'publisher']))) {
          $response['message'] = 'You do not have permision to publish article';
          return json_encode($response);
        }
        // validate
        $validation = \Validator::make($request->all(), [
            'author' => 'required|string|min:3',
            'title' => 'required|string|min:3',
            'image_source' => 'required|url',
            'content' => 'required|string|min:160',
        ]);

        // retrieve and normalise validation error messages
        if ($validation->fails()) {
            $errors = [];
            $messages = $validation->messages()->toArray();

            foreach ($messages as $message) {
                foreach ($message as $error) {
                    $errors[] = $error;
                }
            }
            $response['message'] = implode(', ', $errors);
        } else {
            $author = $request->input('author');
            $title = $request->input('title');
            $image_source = $request->input('image_source');
            $content = $request->input('content');
            $description = substr($content, 0, 250);
            $filename = preg_replace(['/\s/', '/-/', '/\(/', '/\)/', '/__/'], '_', $title);
            $base_url = url('/');

            try {
                // check if article directory is writable
                $content_dir = base_path().'/resources/views/partials/articles/';
                if (!is_writeable($content_dir)) {
                    $response['message'] = 'content directory is not writable';
                } else {
                    // check if file with same name exist
              $file = $content_dir.$filename.'.blade.php';
                    if (file_exists($file)) {
                        $filename = $filename.mt_rand();
                        $file = $content_dir.$filename.'.blade.php';
                    }

              // add entry to database
              Article::insert($author, $title, $image_source, $filename, $description, $base_url);

              // save content to file
              file_put_contents($file, $content);

              //set response
              $response = ['success' => true, 'message' => 'Article successfully published.'];
                }
            } catch (Exception $e) {
                Log::error(__METHOD__.' - '.$e->getMessage());
                $response['message'] = 'Exception error';
            }
        }

        return json_encode($response);
    }

    /**
     * deleteArticle .
     *
     * @return Json {success:boolean, message:string}
     */
    public function deleteArticle($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        $response = ['success' => false, 'message' => 'failed'];

        // retrieve role by id
        $article = Article::find($id);

        // delete article
        if (!$article) {
            $response['message'] = 'Article does not exist';
        } elseif ($article->delete()) {
            $response = ['success' => true, 'message' => 'Article deleted'];
        } else {
            $response['message'] = 'error deleting article';
        }

        return json_encode($response);
    }

    /**
     * [editParticle description].
     *
     * @param string $value [description]
     *
     * @return [type] [description]
     */
    public function editArticle($id)
    {
        // check if user is authenticated
        $this->isAuthorized();

        // get admin users
        $article = Article::find($id);

        if (\Request::isMethod('post')) {

          // TODO: do some validation

          // save content
          $content = \Request::input('content');
          $content_dir = base_path().'/resources/views/partials/articles/';
          $file = $content_dir.$article->filename.'.blade.php';

          if (is_writable($file)){
            file_put_contents($file, $content);
            \Request::session()->flash('notice', 'Article has been updated');
          }
          else
            \Request::session()->flash('notice', 'File is not writable');
        }

        return view('edit_article', ['article' => $article]);
    }
}
