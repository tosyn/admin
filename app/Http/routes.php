<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
  Route::get('/', 'AdminController@index');
  Route::get('/dashboard', 'AdminController@index');

  Route::get('/role', 'AdminController@role');
  Route::get('/page', 'AdminController@page');
  Route::get('/enquiry', 'AdminController@enquiry');
  Route::get('/article', 'AdminController@article');
  Route::get('/article/{id}', 'AdminController@article');
  Route::match(array('get', 'post'), '/login', 'AdminController@login');
  Route::get('/logout', 'AdminController@logout');
  Route::get('/user', 'AdminController@user');
  Route::get('/suspendUser/{id}', 'AdminController@suspendUser');
  Route::get('/activateUser/{id}', 'AdminController@activateUser');
  Route::get('/deleteUser/{id}', 'AdminController@deleteUser');
  Route::match(['get', 'post'], '/updateUser', 'AdminController@updateUser');
  Route::post('/addUser', 'AdminController@addUser');
  Route::get('/deleteUserBatch', 'AdminController@deleteUserBatch');
  Route::get('/listRoles', 'AdminController@listRoles');
  Route::match(['get', 'post'], '/updatePage', 'AdminController@updatePage');
  Route::post('/addRole', 'AdminController@addRole');
  Route::get('/deleteRole/{id}', 'AdminController@deleteRole');
  Route::match(['get', 'post'], '/updateRole', 'AdminController@updateRole');
  Route::get('/deleteRoleBatch', 'AdminController@deleteRoleBatch');
  Route::match(['get', 'post'], '/updateArticle', 'AdminController@updateArticle');
  Route::get('/deleteArticleBatch', 'AdminController@deleteArticleBatch');
  Route::get('/deleteArticle/{id}', 'AdminController@deleteArticle');
  Route::post('/publish', 'AdminController@publish');
  Route::any('/editArticle/{id}', 'AdminController@editArticle');

  Route::get('/{article_id}/{file}', 'AdminController@story');
});
