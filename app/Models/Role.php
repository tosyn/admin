<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * create new role
     * @param  string $name - role name
     * @param  string $note - role description
     * @throws Exception
     * @return Object       [Role]
     */
    public static function insert($name, $note)
    {
        $role = new Role;
        $role->name = $name;
        $role->note = $note;
        $role->save();

        return $role;
    }
}
