<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    /**
     * getDates - override internal getDates]
     * @return void
     */
    public function getDates()
    {
        // only this field will be converted to Carbon
        return [];
    }

    /**
     * Get the phone record associated with the user.
     */
    public function adminRole()
    {
        return $this->hasOne('App\Models\AdminRole');
    }

    /**
     * [insert - add new entry]
     * @param  string $username         [description]
     * @param  password $password         [description]
     * @param  password $active           [description]
     * @param  integer $activation_token [description]
     * @return Admin
     */
    public static function insert($username, $password, $active=true, $activation_token=null)
    {
        $user = new Admin;
        $user->username = $username;
        $user->hash = password_hash($password, PASSWORD_BCRYPT, ['cost'=>12]);
        $user->active = $active;
        $user->activation_token = $activation_token;
        $user->save();

        return $user;
    }
}
