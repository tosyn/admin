<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Article;

class Article extends Model
{
    /**
     * getDates
     * @return null
     */
    public function getDates()
    {
        // only this field will be converted to Carbon
        return [];
    }

    /**
     * insert add new article to the database
     * @param  [string] $author       [description]
     * @param  [string] $title        [description]
     * @param  [string] $image_source [description]
     * @param  [string] $filename     [description]
     * @param  [string] $description  [description]
     * @param  [string] $base_url     [description]
     * @return article
     */
    public static function insert($author, $title, $image_source, $filename, $description, $base_url)
    {
        $article = new Article;

        $id = mt_rand();
        $article->id = $id;
        $article->author = $author;
        $article->title = $title;
        $article->description = $description;
        $article->image_source = $image_source;
        $article->filename = $filename;
        $article->url = $base_url."/$id/$filename";
        $article->save();

        return $article;
    }

}
