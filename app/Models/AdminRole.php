<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{

    protected $table = 'admin_roles';

    public $timestamps = false;

    /**
     * Get the phone record associated with the user.
     */
    public function role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }

    /**
     * [insert description]
     * @param  [integer] $admin_id [description]
     * @param  [integer] $role_id  [description]
     * @return [noolean]           [description]
     */
    public static function insert($admin_id, $role_id)
    {
        $user_role = new AdminRole;
        $user_role->admin_id = $admin_id;
        $user_role->role_id = $role_id;

        $user_role->save();
        return $user_role;

    }
}
