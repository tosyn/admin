$(document).ready(function() {

  // auto remove alert container
  setInterval(function() {
    $('.alert').slideUp("slow", function() {
      $('.alert').remove();
    });
  }, 5000);

  // get csrf token - required for updating record
  var token = $("input[name=_token]").val();

  // Initialise user DataTable
  var userTable = $('#user').dataTable();

  // make user table editable
  userTable.makeEditable({
    sUpdateURL: "/updateUser",
    oUpdateParameters: {
      _token: token
    },
    fnShowError: function(result, action) {
      var data = jsonParse(result);
      if (data.success) {
        sessionStorage.notice = data.message;
        document.location.reload();
      } else
        displayAlert(data.message, alertPosition);
    },
    "aoColumns": [
      null, {
        indicator: 'Updating user role ...',
        tooltip: 'Double click to edit',
        type: 'select',
        loadurl: '/listRoles',
        onblur: 'submit',
      },
    ]
  });

  // Pop handler
  $('.open-popup-link').magnificPopup({
    type: 'inline',
  });

  /** attach a submit handler to add-user form **/
  $("#add-user").submit(function(e) {

    // get form data
    var postData = $(this).serializeArray();

    /* Send the data */
    var response = ajaxCall($(this).attr("action"), postData, $(this).attr("method"));

    // process response
    response.success(function(result) {
      if (result.success === true) {
        sessionStorage.notice = result.message;
        document.location.reload();
      } else {
        displayAlert(result.message, 'form');
      }
    }).error(function(err) {
      displayAlert(err.statusText, 'form');
    });

    /* stop form from submitting normally */
    e.preventDefault();
  });

  // Initialise enquiry datatable
  $('#enquiry').dataTable();

  // Initialise page datatable
  var pageTable = $('#page').dataTable();

  // make page table editable
  pageTable.makeEditable({
    sUpdateURL: "/updatePage",
    oUpdateParameters: {
      _token: token
    },
    fnShowError: function(result, action) {
      var data = jsonParse(result);
      if (data.success) {
        sessionStorage.notice = data.message;
        document.location.reload();
      } else
        displayAlert(data.message, alertPosition);
    },
    "aoColumns": [
      null,
      null, {
        indicator: 'Updating page note ...',
        tooltip: 'Double click to edit note',
        type: 'textarea',
        submit: 'SAVE',
      },
    ]
  });

  // Initialise role datatable
  var roleTable = $('#role').dataTable();

  // make role table editable
  roleTable.makeEditable({
    sUpdateURL: "/updateRole",
    oUpdateParameters: {
      _token: token
    },
    fnShowError: function(result, action) {
      var data = jsonParse(result);
      if (data.success) {
        sessionStorage.notice = data.message;
        document.location.reload();
      } else
        displayAlert(data.message, alertPosition);
    },
    "aoColumns": [
      null,
      null, {
        indicator: 'Updating role note ...',
        tooltip: 'Double click to edit note',
        onblur: 'submit',
      },
    ]
  });

  /** attach a submit handler to new role form **/
  $("#new-role").submit(function(e) {

    // get form data
    var postData = $(this).serializeArray();

    /* Send the data */
    var response = ajaxCall($(this).attr("action"), postData, $(this).attr("method"));

    // process response
    response.success(function(result) {
      if (result.success === true) {
        sessionStorage.notice = result.message;
        document.location.reload();
      } else {
        displayAlert(result.message, 'form');
      }
    }).error(function(err) {
      displayAlert(err.statusText, 'form');
    });

    /* stop form from submitting normally */
    e.preventDefault();
  });

  // Initialise page datatable
  var pageTable = $('#page').dataTable();

  // make page table editable
  pageTable.makeEditable({
    sUpdateURL: "/updatePage",
    oUpdateParameters: {
      _token: token
    },
    fnShowError: function(result, action) {
      var data = jsonParse(result);
      if (data.success) {
        sessionStorage.notice = data.message;
        document.location.reload();
      } else
        displayAlert(data.message, alertPosition);
    },
    "aoColumns": [
      null,
      null, {
        indicator: 'Updating page note ...',
        tooltip: 'Double click to edit note',
        type: 'textarea',
        submit: 'SAVE',
      },
    ]
  });

  // Initialise article datatable
  var articleTable = $('#article').dataTable();

  // make article table editable
  articleTable.makeEditable({
    sUpdateURL: "/updateArticle",
    oUpdateParameters: {
      _token: token
    },
    fnShowError: function(result, action) {
      var data = jsonParse(result);
      if (data.success) {
        sessionStorage.notice = data.message;
        document.location.reload();
      } else
        displayAlert(data.message, alertPosition);
    },
    "aoColumns": [
      null,
      {
        indicator: 'Updating article author ...',
        tooltip: 'Double click to edit',
        submit: 'save'
      },
      {
        indicator: 'Updating article title ...',
        tooltip: 'Double click to edit',
        submit: 'save'
      },
      {
        indicator: 'Updating article image source ...',
        tooltip: 'Double click to edit',
        submit: 'save'
      },
      {
        indicator: 'Updating article ...',
        tooltip: 'Double click to edit article',
        type: 'textarea',
        submit: 'save',
      },
    ]
  });

  /** attach a submit handler to new article form **/
  $("#new-article").submit(function(e) {

    // get form data
    var postData = $(this).serializeArray();

    /* Send the data */
    var response = ajaxCall($(this).attr("action"), postData, $(this).attr("method"));

    // process response
    response.success(function(result) {
      if (result.success === true) {
        sessionStorage.notice = result.message;
        document.location.reload();
      } else {
        displayAlert(result.message, 'form');
      }
    }).error(function(err) {
      displayAlert(err.statusText, 'form');
    });

    /* stop form from submitting normally */
    e.preventDefault();
  });

});

// default alert position -
// alert will be placed before this element
var alertPosition = '.dataTables_wrapper';

/**
 * Convert json string to object
 * Catch badly formed json string
 * @param {string} json - valid json string
 */
function jsonParse(json) {
  try {
    return data = JSON.parse(json);
  } catch (e) {
    console.log('json_error: ' + e);
    return data = {
      'success': false,
      'message': 'Invalid json string'
    };
  }
}

/** display notice (if any) on page reload **/
$(function() {
  if (sessionStorage.notice) {
    displayAlert(sessionStorage.notice, alertPosition);
    sessionStorage.removeItem('notice');
  }
})

/**
 * var msg - message to display
 * var place - where to place the alert box (default is '.dataTables_wrapper'))
 */
function displayAlert(msg, position) {
  // remve existing error message
  $('.alert').remove();

  // display error me
  $(position).before("<div class='alert alert-warning alert-dismissible alert-small' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + msg + "</div>");
}

/**
 * make ajax call
 **/
function ajaxCall(url, data, method) {
  return $.ajax({
    url: url,
    type: method,
    data: data,
    cache: false,
    dataType: 'json',
  });
};

// suspend user
function suspendUser(id) {
  ajaxCall('/suspendUser/' + id, [], 'GET').success(function(response) {
    if (response.success) {
      sessionStorage.notice = response.message;
      document.location.reload();
    } else
      displayAlert(response.message, alertPosition);
  }).error(function(err) {
    displayAlert(err.statusText, alertPosition);
  });
};

// activate suspended user
function activateUser(id) {
  ajaxCall('/activateUser/' + id, [], 'GET').success(function(response) {
    if (response.success) {
      sessionStorage.notice = response.message;
      document.location.reload();
    } else
      displayAlert(response.message, alertPosition);
  }).error(function(err) {
    displayAlert(err.statusText, alertPosition);
  });
};

// delete user
function deleteUser(id) {
  ajaxCall('/deleteUser/' + id, [], 'GET').success(function(response) {
    if (response.success) {
      sessionStorage.notice = response.message;
      document.location.reload();
    } else
      displayAlert(response.message, alertPosition);
  }).error(function(err) {
    displayAlert(err.statusText, alertPosition);
  });
};

// delete role
function deleteRole(id) {
  ajaxCall('/deleteRole/' + id, [], 'GET').success(function(response) {
    if (response.success) {
      sessionStorage.notice = response.message;
      document.location.reload();
    } else
      displayAlert(response.message, alertPosition);
  }).error(function(err) {
    displayAlert(err.statusText, alertPosition);
  });
};

// role batch delete
function deleteRoleBatch() {
  // get all selected checkboxes
  var selected = [];
  $('#role input:checked').each(function() {
    selected.push($(this).attr('data-role-id'));
  });

  // check selected roles
  if (selected.length === 0) {
    displayAlert('Please select role(s) to delete.', alertPosition);
  } else {
    // delete selected roles
    ajaxCall('/deleteRoleBatch', {
      ids: selected
    }, 'GET').success(function(response) {
      if (response.success) {
        sessionStorage.notice = response.message;
        document.location.reload();
      } else
        displayAlert(response.message, alertPosition);
    }).error(function(err) {
      displayAlert(err.statusText, alertPosition);
    });
  }
};

// user batch delete
function deleteUserBatch() {
  // get all selected checkboxes
  var selected = [];
  $('#user input:checked').each(function() {
    selected.push($(this).attr('data-user-id'));
  });

  // check selected user
  if (selected.length === 0) {
    displayAlert('Please select user(s) to delete.', alertPosition);
  } else {
    // delete selected users
    ajaxCall('/deleteUserBatch', {
      ids: selected
    }, 'GET').success(function(response) {
      if (response.success) {
        sessionStorage.notice = response.message;
        document.location.reload();
      } else
        displayAlert(response.message, alertPosition);
    }).error(function(err) {
      displayAlert(err.statusText, alertPosition);
    });
  }
};

// articles batch delete
function deleteArticleBatch() {
  // get all selected checkboxes
  var selected = [];
  $('#article input:checked').each(function() {
    selected.push($(this).attr('data-article-id'));
  });

  // check selected user
  if (selected.length === 0) {
    displayAlert('Please select article(s) to delete.', alertPosition);
  } else {
    // delete selected articles
    ajaxCall('/deleteArticleBatch', {
      ids: selected
    }, 'GET').success(function(response) {
      if (response.success) {
        sessionStorage.notice = response.message;
        document.location.reload();
      } else
        displayAlert(response.message, alertPosition);
    }).error(function(err) {
      displayAlert(err.statusText, alertPosition);
    });
  }
};

// delete article
function deleteArticle(id) {
  ajaxCall('/deleteArticle/' + id, [], 'GET').success(function(response) {
    if (response.success) {
      sessionStorage.notice = response.message;
      document.location.reload();
    } else
      displayAlert(response.message, alertPosition);
  }).error(function(err) {
    displayAlert(err.statusText, alertPosition);
  });
};
