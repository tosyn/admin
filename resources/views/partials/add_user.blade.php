<div id="add-user-dialog" class="white-popup mfp-hide">

  <h3>New User</h3>
  <hr />

  <form id="add-user" class="form-horizontal" action="/addUser" method="POST">

    {!! csrf_field() !!}

    <div class="form-group">
      <div class="col-sm-10">
        <input type="text" name="username" class="form-control" id="username" placeholder="Username">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <select name="role_id" class="form-control">
          <!-- TODO: dynamically get this data -->
          <option value="" selected="">Select Role</option>
          <option value="1">Admin</option>
          <option value="2">Support</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <input type="password" name="password_confirmation" class="form-control" id="password" placeholder="Confirm Password">
      </div>
    </div>

    <button type="submit" class="btn btn-default">ADD</button>
  </form>

</div>
