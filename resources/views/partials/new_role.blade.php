<div id="new-role-dialog" class="white-popup mfp-hide">

  <h3>New Role</h3>
  <hr />

  <form id="new-role" class="form-horizontal" action="/addRole" method="POST">

    {!! csrf_field() !!}

    <div class="form-group">
      <div class="col-sm-10">
        <input type="text" name="name" class="form-control" id="name" placeholder="Role name">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <textarea name="note" class="form-control" placeholder="Description"></textarea>
      </div>
    </div>

    <button type="submit" class="btn btn-default">SAVE</button>
  </form>

</div>
