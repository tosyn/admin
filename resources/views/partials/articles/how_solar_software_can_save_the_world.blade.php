<article>
  <p>
    VCs have avoided solar deals ever since Solyndra became a four-letter word. But while their attention has strayed, the industry has been on a tear. In 2010, U.S. solar installers hit a milestone of 1 GW per year. Five years later, they’re installing more than 1 GW per month.
  </p>

  <p>
    This tremendous growth has fed a swelling herd of solar unicorns populated by the likes of SolarCity, SunEdison, SunPower and more.
  </p>


  <p>
    Recently, the industry has been buffeted by a variety of tailwinds that should drive even faster expansion. The landmark Paris climate accord promises stronger regulatory support across the world. Concurrently, a group of billionaires led by Bill Gates announced the Breakthrough Energy Coalition to fund this roll out.
  </p>

  <p>
    And the U.S. Congress has extended the solar Investment Tax Credit (ITC), which has raised installation forecasts through 2020 by more than 50 percent. Add to this mix innovation in large-scale battery manufacturing and the future of distributed power generation looks bright indeed.
  </p>

  <p>
    It’s also creating an opportunity to build the first SaaS unicorn focused on distributed generation. As this industry grows, so does the need for software to improve efficiency and lower costs. “Soft costs,” like permitting, financing and customer acquisition, now represent roughly two-thirds of installed costs of residential deployments. As I’ve written about before, the best way to address such soft costs is with software.
  </p>

  <p>
    As an example, the manner in which solar developers identify, track and quote potential customers today is decades behind other industries. The leading solar players today use a “spit and glue” combination of Salesforce, homegrown code and Excel. It’s not shocking, therefore, that SolarCity’s customer acquisition costs have actually increased year over year, while installation costs have plummeted.
  </p>

</article>
