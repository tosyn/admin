<article>
  <p>
    Without any notice, Blue Origin made history once again when they completed a successful launch and landing of a suborbital rocket yesterday. What made this flight particularly notable was that they used the same rocket that had been launched into space on a flight in November.
  </p>

  <p>
    The recovered New Shepard rocket launched from Blue Origin’s test site in West Texas, brought a capsule to space at an altitude of 101.7 kilometers, and then both the booster and the capsule returned safely back down to the ground. The Karman line, the generally agreed upon line of space, is held at 100 kilometers.
  </p>
  <p>
    The booster was directed back to the Earth where it made a propulsive soft landing. The capsule returned at a different location using 3 large parachutes and retro-thrust propulsion to slow its descent.
  </p>

  <p>
    Video of the launch was titled “Launch. Land. Repeat.” A simple concept, but a truly revolutionary strategy for the rocket industry that is just now taking shape with companies like Blue Origin and SpaceX leading the way.
  </p>

  <p>
    In November, Blue Origin performed a similar suborbital launch and landing with the New Shepard rocket to an altitude of 100.5 kilometers.
  </p>

  <p>
    The New Shephard vehicle will be used to bring tourists and researchers into suborbital space, competing with similar rides offered by Richard Branson’s company, Virgin Galactic.
  </p>
    <p>
    In a blog post on the company’s website, Blue Origin founder Jeff Bezos explained that a few hardware replacements and software adjustments were necessary after their first New Shepard launch. In addition to replacing the crew capsule parachutes and the booster’s pyro igniters, the Blue Origin team made a “noteworthy” software improvement related to landing on the pad.
  </p>
  <p>
    Back in December, SpaceX successfully launched and landed their Falcon 9 rocket in an orbital mission to space. Many, including Elon Musk himself, were quick to point out that SpaceX’s rocket recovery was slightly different than Jeff Bezos’ rocket recovery.
  </p>
</article>
