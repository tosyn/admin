<article>
  <p>
    If we trace a path that starts with Gutenberg’s use of moveable type to Malcom McLean’s invention of the shipping container, we start to recognize a very interesting pattern: Each new layer of abstraction and standardization creates tremendous value out of the resulting increases in scale and efficiency.
  </p>

  <p>
    Today’s digital innovators can trace a similar historical path that starts with mainframe computers and monolithic applications and then, step-by-step, reveals software’s interchangeable parts until we arrive at today’s cloud-based era of microservices and continuous integration.
  </p>

  <p>
    Microservices is an approach to building software that shifts away from large monolithic applications toward small, loosely coupled and composable autonomous pieces. The benefit of this abstraction is specialization, which drives down costs to develop and drives up agility and quality — while operating much more resilient systems.
  </p>


  <p>
    This approach is not new; Amazon and Google have been using it for more than a decade. Container technologies (like Docker), APIs and the availability of cloud infrastructure have made microservices something that is now practical for a much broader set of companies.
  </p>

  <p>
    Companies like Airbnb, Disney, Dropbox, GE, Goldman Sachs and Twitter have seen development lead times cut by as much as 75 percent when using microservices. The concept sounds simple enough, but doing it is harder than it looks. This has given rise to a whole new ecosystem of companies and open-source software to help people with this transition.
  </p>
</article>
