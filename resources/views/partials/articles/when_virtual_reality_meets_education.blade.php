<article>
  <p>
Since the 1950s, virtual reality (VR) has been hovering on the periphery of technology without achieving accepted mainstream application or commercial adoption. Since 2012, VR startups have raised more than $1.46 billion in venture capital, including more than $100 million in funding during the last four consecutive quarters.
  </p>

  <p>
According to Citi analyst Kota Ezawa, 2016 is the year that VR will take off in earnest, with the VR market expected to grow to a $15.9 billion industry by 2019. Citi also anticipates the market for hardware, networks, software and content will reach $200 billion by 2020.
  </p>

  <p>
The content share of this market is of particular interest, as this segment of the tech industry has historically been dedicated to gaming — but the world is changing. We are shifting from the now relatively benign universe in Aldous Huxley’s Brave New World to Ernest Cline’s VR paradigm as described in Ready Player One. Like Huxley, Cline has written of a dystopian environment wherein technology has overtaken humanity.
  </p>

  <p>
For our purpose, let’s consider VR as a useful tool, and perhaps even a productive enhancement to human interaction, bringing together people from around the world to engage and interact — regardless of social, economic or geographic disparities. In the abstract as well as the applied, modern education is poised to take advantage of this latest tech innovation.
  </p>

  <p>
    Over the last several years, VR has moved from being the purview of the military and aviation to the mainstream of professional development, as managers, instructors, coaches and therapists have claimed increasing benefit from immersive experiences.
  </p>

  <p>
While statistics on VR use in K-­12 schools and colleges have yet to be gathered, the steady growth of the market is reflected in the surge of companies (including zSpace, Alchemy VR and Immersive VR Education) solely dedicated to providing schools with packaged educational curriculum and content, teacher training and technological tools to support VR­-based instruction in the classroom. Myriad articles, studies and conference presentations attest to the great success of 3D immersion and VR technology in hundreds of classrooms in educationally progressive schools and learning labs in the U.S. and Europe.
  </p>
</article>
