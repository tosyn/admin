<div id="new-article-dialog" class="white-popup mfp-hide wide-popup">

  <h3>Publish New Article</h3>
  <hr />

  <form id="new-article" class="form-horizontal" action="/publish" method="POST">

    {!! csrf_field() !!}

    <div class="form-group">
      <div class="col-sm-10">
        <input type="text" name="author" class="form-control" id="author" placeholder="Author name">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <input type="text" name="title" class="form-control" id="title" placeholder="Title">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <input type="text" name="image_source" class="form-control" id="name" placeholder="Image Source">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-10">
        <textarea name="content" class="form-control" placeholder="content"></textarea>
      </div>
    </div>

    <button type="submit" class="btn btn-default">PUBLISH</button>
  </form>

</div>
