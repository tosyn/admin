@extends('layout')

@section('title', '| article title')

@section('content')

  @if ($article)

    <div class="content">

      <div class="header">
          <div class="header-title">
              <a href="/">Dashboard</a> > Editing :: {{$article->title}}
          </div>

          <div class="header-actions pull-right">
            <a class="btn btn-default" href="/article"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> </span> Back to Articles</a>

          </div>
          <hr />
      </div>
      @if (Session::has('notice'))
        <i class="text-danger">{{ Session::get('notice') }}</i>
      @endif
          <form id="edit-article" class="form-horizontal" action="/editArticle/{{$article->id}}" method="POST">

            {!! csrf_field() !!}

            <div class="form-group">
              <div class="col-sm-10">
                <textarea name="content" class="form-control" placeholder="content" style="height:400px;">
                  {!! file_get_contents($article->url) !!}
                </textarea>
              </div>
            </div>

            <button type="submit" class="btn btn-default">SAVE</button>
          </form>
      </div>
  @endif

@endsection
