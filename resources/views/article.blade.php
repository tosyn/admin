@extends('layout')

@section('title', '| Articles')

@section('content')

  <div class="content">
    <div class="header">
        <div class="header-title">
            <a href="/">Dashboard</a> > {{$title}}
        </div>
        <div class="header-actions pull-right">
          <a href="#new-article-dialog" role="button" class="open-popup-link btn btn-default" aria-label="Left Align" title="publish new article">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"> </span> PUBLISH
          </a>

          <button class="btn btn-default" type="submit" onclick="deleteArticleBatch()"><span class="glyphicon glyphicon-trash" aria-hidden="true"> </span> Delete Selected</button>
        </div>

        <hr />
    </div>

    @if ($articles)

      <table id="article" class="table table-responsive table-striped">
        <thead>
            <th>S/N</th>
            <th>Author</th>
            <th>Title</th>
            <th>Image Source</th>
            <th>Description</th>
            <th>Date</th>
            <th class="action">Action</th>
        </thead>
        <tbody>
          @foreach ($articles as $article)
            <tr id="{{ $article->id }}">
                <td><input type="checkbox" data-article-id="{{$article->id}}"></td>
                <td style="text-transform:capitalize;">{{$article->author}}</td>
                <td>{{$article->title}}</td>
                <td>{{$article->image_source}}</td>
                <td>{{$article->description}}</td>
                <td>{{date('M d Y', strtotime($article->created_at))}}</td>
                <td class="action">
                    <a href="/editArticle/{{$article->id}}" title="edit article"><span title="edit article content" class="glyphicon glyphicon-pencil"></a>

                    <a href="#" onclick="deleteArticle('{{$article->id}}')"><span title="delete article" class="glyphicon glyphicon-trash"></a>
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>

  <!-- add publish dialog -->
  @include('partials.publish')

@endsection
