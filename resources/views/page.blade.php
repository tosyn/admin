@extends('layout')

@section('title', '| Pages')

@section('content')

  <div class="content">
    <h3><a href="/">Dashboard</a> > {{$title}}</h3>
    <hr />

    @if ($pages)

      <table id="page" class="table table-striped">
        <thead>
            <th>Name</th>
            <th>Title</th>
            <th>Note</th>
        </thead>
        <tbody>
          @foreach ($pages as $page)
            <tr id="{{$page->id}}">
                <td>{{$page->name}}</td>
                <td>{{$page->title}}</td>
                <td>{{$page->note}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>


@endsection
