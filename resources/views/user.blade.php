@extends('layout')

@section('title', '| Home')

@section('content')

  <div class="content">
    <div class="header">
        <div class="header-title">
            <a href="/">Dashboard</a> > {{$title}}
        </div>
        <div class="header-actions pull-right">
          <a href="#add-user-dialog" role="button" class="open-popup-link btn btn-default" aria-label="Left Align" title="add new user">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"> </span> NEW
          </a>

          <button class="btn btn-default" type="submit" onclick="deleteUserBatch()"><span class="glyphicon glyphicon-trash" aria-hidden="true"> </span> Delete Selected</button>
        </div>

        <hr />
    </div>


    @if ($users)

      <table id="user" class="table table-striped">
        <thead>
            <th>S/N</th>
            <th>Username</th>
            <th>Role</th>
            <th>Active</th>
            <th>Created</th>
            <th class="action">Action</th>
        </thead>
        <tbody>
          @foreach ($users as $user)
            <tr id="{{$user->id}}">
                <td><input type="checkbox" data-user-id="{{$user->id}}"></td>
                <td>{{$user->username}}</td>
                <td>
                  @if (($user->adminRole))
                    {{$user->adminRole->role->name}}
                  @endif
                </td>
                <td>
                  @if ($user->active)
                    <i class="glyphicon glyphicon-ok"></i>
                  @else
                    <i class="glyphicon glyphicon-remove"></i>
                  @endif
                </td>
                <td>{{date('d-m-Y', strtotime($user->created_at))}}</td>
                <td class="action">
                  @if ($user->active)
                    <a href="#" onclick="suspendUser('{{$user->id}}')"><span title="suspend user" class="glyphicon glyphicon-ban-circle"></span></a>
                  @else
                    <a href="#" onclick="activateUser('{{$user->id}}')"><span title="activate user" class="glyphicon glyphicon-ok-circle"></span></a>
                  @endif

                  <a href="#" onclick="deleteUser('{{$user->id}}')"><span title="delete user" class="glyphicon glyphicon-trash"></a>
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>

  <!-- add user dialog -->
  @include('partials.add_user')

@endsection
