@extends('layout')

@section('title', '| Enquiry')

@section('content')

  <div class="content">
    <h3><a href="/">Dashboard</a> > {{$title}}</h3>
    <hr />

    @if ($enquiries)

      <table id="enquiry"  class="table table-responsive table-striped">
        <thead>
            <th>S/N</th>
            <th>Name</th>
            <th>Email</th>
            <th>Subject</th>
            <th class="col-md-6">Message</th>
            <th>Date</th>
        </thead>
        <tbody>
          @foreach ($enquiries as $enquiry)
            <tr>
                <td><input type="checkbox" data-role-id="{{$enquiry->id}}" disabled></td>
                <td>{{$enquiry->name}}</td>
                <td>{{$enquiry->email}}</td>
                <td>{{$enquiry->subject}}</td>
                <td>{{$enquiry->message}}</td>
                <td>{{date('d M Y', strtotime($enquiry->created_at))}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>


@endsection
