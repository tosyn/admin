@extends('layout')

@section('title', '| Home')

@section('content')

  <div class="content">
    <h3>{{$title}}</h3>
    <hr />
    <div class="row">

      <div class="col-md-2">
        <div class="jumbotron">
          <span class="glyphicon glyphicon-user glyphicon-lg" aria-hidden="true"></span>
          <a href="/user"><h2>Users</h2></a>
        </div>
      </div>

      <div class="col-md-2">
        <div class="jumbotron">
          <span class="glyphicon glyphicon-tasks glyphicon-lg" aria-hidden="true"></span>
          <a href="/role"><h2>Roles</h2></a>
        </div>
      </div>

      <div class="col-md-2">
        <div class="jumbotron">
          <span class="glyphicon glyphicon-list-alt glyphicon-lg" aria-hidden="true"></span>
          <a href="/page"><h2>Pages</h2></a>
        </div>
      </div>

      <div class="col-md-2">
        <div class="jumbotron">
          <span class="glyphicon glyphicon-envelope glyphicon-lg" aria-hidden="true"></span>
          <a href="/enquiry"><h2>Enquiries</h2></a>
        </div>
      </div>

      <div class="col-md-2">
        <div class="jumbotron">
          <span class="glyphicon glyphicon-briefcase glyphicon-lg" aria-hidden="true"></span>
          <a href="/article"><h2>Articles</h2></a>
        </div>
      </div>

    </div>

  </div>

@endsection
