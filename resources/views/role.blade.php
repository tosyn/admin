@extends('layout')

@section('title', '| Roles')

@section('content')

  <div class="content">
    <div class="header">
        <div class="header-title">
            <a href="/">Dashboard</a> > {{$title}}
        </div>

        @if ($roles)
          <div class="header-actions pull-right">

            <a href="#new-role-dialog" role="button" class="open-popup-link btn btn-default" aria-label="Left Align" title="add new role">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"> </span> New Role
            </a>

            <button class="btn btn-default" type="submit" onclick="deleteRoleBatch()"><span class="glyphicon glyphicon-trash" aria-hidden="true"> </span> Delete Selected</button>

          </div>
        @endif

        <hr />
    </div>

    @if ($roles)

      <table id="role" class="table table-striped">
        <thead>
            <th>S/N</th>
            <th>Name</th>
            <th>Note</th>
            <th class="action">Action</th>
        </thead>
        <tbody>
          @foreach ($roles as $role)
            <tr id="{{$role->id}}">
                <td><input type="checkbox" data-role-id="{{$role->id}}"></td>
                <td>{{$role->name}}</td>
                <td>{{$role->note}}</td>
                <td class="action">
                  <a href="#" onclick="deleteRole('{{$role->id}}')"><span title="delete role" class="glyphicon glyphicon-trash"></a>
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>

    @endif
  </div>

  <!-- new role dialog -->
  @include('partials.new_role')

@endsection
