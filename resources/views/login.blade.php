@extends('layout')

@section('title', '| Login')

@section('content')

  <div class="content login">
    <h3>Login</h3>


    <div class="row">

      <div class="col-md-6">
          <hr />
          <form id="login-form" class="form-horizontal" action="/login" method="POST">
            @if (Session::has('notice'))
              <i class="text-danger">{{ Session::get('notice') }}</i>
            @endif

            {!! csrf_field() !!}

            <div class="form-group">
              <div class="col-sm-10">
                @if ($errors->has('username'))
                  <i class="text-warning">{{ $errors->first('username') }}</i>
                @endif
                <input type="text" name="username" class="form-control" id="username" placeholder="Username">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-10">
                @if ($errors->has('password'))
                  <i class="text-warning">{{ $errors->first('password') }}</i>
                @endif
                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
              </div>
            </div>

            <button type="submit" class="btn btn-default">LOGIN</button>
          </form>

      </div>
    </div>

  </div>

@endsection
