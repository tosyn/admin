<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="ogilvy test blog">
        <meta name="author" content="tosyn">

        <title>Ogilvy Blog @yield('title')</title>

        <!-- font -->
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- DataTable -->
        <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

        <!-- bootstrap css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- magnific pop css -->
        <link href="/css/magnific-popup.css" rel="stylesheet">

        <!-- custom css -->
        <link href="/css/main.css" rel="stylesheet">

    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">Blog Admin</a>
            </div>

            <ul class="nav navbar-nav pull-right right-nav">
              @if (Session::has('username'))
                <li><a>Welcome {{Session::get('username')}} <span class="glyphicon glyphicon-user
"> </span></a> </li>
                <li><a href="/logout">Logout</a></li>
              @else
                <li><a href="/login">Login </a></li>
              @endif
            </ul>

          </div>
        </nav>


        <div class="container">
            <!-- required for table update -->
            {!! csrf_field() !!}

           @yield('content')
        </div>

        <footer class="footer">
           <div class="container">
             <p>Test blog admin built for <a href="http://www.ogilvy.co.za" target="_blank">Ogilvy</a> by <a href="https://twitter.com/Roncadel" target="_blank">@Roncadel</a>.</p>
           </div>
         </footer>

        <!-- JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>

        <!-- DataTable -->
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

        <!-- make DataTable editable -->
        <script src="/js/jquery.dataTables.editable.js"></script>
        <script src="/js/jquery.jeditable.mini.js"></script>

        <!-- Magnific popup js -->
        <script src="/js/jquery.magnific-popup.min.js"></script>

        <!-- main js -->
        <script src="/js/main.js"></script>
    </body>
</html>
